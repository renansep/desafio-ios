//
//  RepositoriesViewController.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "RepositoriesTableViewController.h"

#import "RepositoryNetworking.h"
#import "RepositoryCell.h"
#import "Repository.h"
#import "PullRequestsTableViewController.h"

typedef enum : NSUInteger {
    RepositoriesControllerStateLoading = 0,
    RepositoriesControllerStateContent = 1,
    RepositoriesControllerStateError = 2
} RepositoriesControllerState;

@interface RepositoriesTableViewController ()

@property (strong, nonatomic) NSArray *repositories;

@property (assign, nonatomic) RepositoriesControllerState state;

@property (assign, nonatomic) NSUInteger page;
@property (assign, nonatomic) BOOL reachedLastPage;

@end

@implementation RepositoriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] init];
    backBarButtonItem.title = @"";
    self.navigationItem.backBarButtonItem = backBarButtonItem;
    
    self.page = 1;
    self.reachedLastPage = NO;
    
    [self.tableView registerNibForCellWithClass:[RepositoryCell class]];
    self.tableView.estimatedRowHeight = 125.f;
    
    self.state = RepositoriesControllerStateLoading;
    [self loadRepositories];
}

- (void)setRepositories:(NSArray *)repositories {
    _repositories = repositories;
    self.state = _repositories.count > 0 ? RepositoriesControllerStateContent : RepositoriesControllerStateError;
}

- (void)setState:(RepositoriesControllerState)state {
    _state = state;
    [self.tableView reloadData];
}

- (void)loadRepositories {
    [RepositoryNetworking getRepositoriesWithPage:_page successBlock:^(NSArray *repositories) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.repositories = _page == 1 ? repositories : [_repositories arrayByAddingObjectsFromArray:repositories];
            self.page++;
            self.reachedLastPage = repositories.count == 0;
        });
    } failureBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.state = RepositoriesControllerStateError;
            // TODO: show retry view
        });
    }];
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _state == RepositoriesControllerStateContent ? _repositories.count : 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryCell *cell = [tableView dequeueReusableCellWithClass:[RepositoryCell class] forIndexPath:indexPath];
    if (_state == RepositoriesControllerStateContent) {
        Repository *repository = _repositories[indexPath.row];
        
        cell.style = RepositoryCellStyleContent;
        cell.nameLabel.text = repository.name;
        cell.summaryLabel.text = repository.summary;
        cell.forksCountLabel.text = repository.forksCount.stringValue;
        cell.starsCountLabel.text = repository.stargazersCount.stringValue;
        cell.ownerNameLabel.text = repository.owner.login;
        
        cell.ownerImageView.layer.cornerRadius = cell.ownerImageView.bounds.size.width * (repository.owner.type == GihubUserTypeUser ? 0.5f : 0.f);
        [cell.ownerImageView sd_setImageWithURL:repository.owner.avatarURL placeholderImage:[UIImage imageNamed:@"avatar-icon"]];
    }
    else {
        cell.style = RepositoryCellStylePlaceholder;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_state != RepositoriesControllerStateContent || indexPath.row != _repositories.count - 1 || _reachedLastPage) return;
    
    [self loadRepositories];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Repository *repository = _repositories[indexPath.row];
    [self performSegueWithIdentifier:@"pullRequestsSegue" sender:repository];
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pullRequestsSegue"]) {
        if ([sender isKindOfClass:[Repository class]] && [segue.destinationViewController isKindOfClass:[PullRequestsTableViewController class]]) {
            ((PullRequestsTableViewController *) segue.destinationViewController).repository = sender;
        }
    }
}

@end
