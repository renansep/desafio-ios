//
//  RepositoryCell.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "RepositoryCell.h"

@implementation RepositoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    _ownerImageView.layer.masksToBounds = YES;
    
    _forkImageView.image = [_forkImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starImageView.image = [_starImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)setStyle:(RepositoryCellStyle)style {
    UIColor *backgroundColor;
    BOOL hideViews;
    switch (style) {
        case RepositoryCellStyleContent: {
            backgroundColor = [UIColor clearColor];
            hideViews = NO;
            break;
        }
            
        case RepositoryCellStylePlaceholder: {
            _nameLabel.text = _summaryLabel.text = _ownerNameLabel.text = @" ";
            backgroundColor = RGB(221, 221, 221);
            hideViews = YES;
            break;
        }
    }
    
    _nameLabel.backgroundColor = _summaryLabel.backgroundColor = _ownerNameLabel.backgroundColor = backgroundColor;
    _forksCountLabel.hidden = _starsCountLabel.hidden = hideViews;
}

@end
