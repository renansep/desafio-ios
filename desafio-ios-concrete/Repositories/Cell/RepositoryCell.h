//
//  RepositoryCell.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    RepositoryCellStyleContent = 0,
    RepositoryCellStylePlaceholder = 1
} RepositoryCellStyle;

@interface RepositoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forkImageView;
@property (weak, nonatomic) IBOutlet UILabel *forksCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView;
@property (weak, nonatomic) IBOutlet UILabel *starsCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ownerImageView;
@property (weak, nonatomic) IBOutlet UILabel *ownerNameLabel;

@property (assign, nonatomic) RepositoryCellStyle style;

@end
