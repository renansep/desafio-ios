//
//  PullRequestCell.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    PullRequestCellStyleContent = 0,
    PullRequestCellStylePlaceholder = 1
} PullRequestCellStyle;

@interface PullRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (assign, nonatomic) PullRequestCellStyle style;

@end
