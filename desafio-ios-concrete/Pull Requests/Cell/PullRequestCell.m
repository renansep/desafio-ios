//
//  PullRequestCell.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "PullRequestCell.h"

@implementation PullRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _userImageView.layer.masksToBounds = YES;
    _userImageView.layer.cornerRadius = _userImageView.bounds.size.width / 2;
}

- (void)setStyle:(PullRequestCellStyle)style {
    UIColor *backgroundColor;
    BOOL hideViews;
    
    switch (style) {
        case PullRequestCellStyleContent: {
            backgroundColor = [UIColor clearColor];
            hideViews = NO;
            break;
        }
            
        case PullRequestCellStylePlaceholder: {
            _titleLabel.text = _bodyLabel.text = @" ";
            backgroundColor = RGB(221, 221, 221);
            hideViews = YES;
            break;
        }
    }
    
    _titleLabel.backgroundColor = _bodyLabel.backgroundColor = _userNameLabel.backgroundColor = backgroundColor;
    _userNameLabel.hidden = hideViews;
}

@end
