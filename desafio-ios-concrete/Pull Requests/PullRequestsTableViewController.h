//
//  PullRequestsTableViewController.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Repository;

@interface PullRequestsTableViewController : UITableViewController

@property (weak, nonatomic) Repository *repository;

@end
