//
//  PullRequestsTableViewController.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "PullRequestsTableViewController.h"

#import "RepositoryNetworking.h"
#import "PullRequestCell.h"
#import "PullRequest.h"
#import "Repository.h"

typedef enum : NSUInteger {
    PullRequestsControllerStateLoading = 0,
    PullRequestsControllerStateContent = 1,
    PullRequestsControllerStateError = 2
} PullRequestsControllerState;

@interface PullRequestsTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *openedCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedCountLabel;

@property (strong, nonatomic) NSArray *pullRequests;
@property (assign, nonatomic) NSUInteger openedCount;
@property (assign, nonatomic) NSUInteger closedCount;

@property (assign, nonatomic) PullRequestsControllerState state;

@property (assign, nonatomic) NSUInteger page;
@property (assign, nonatomic) BOOL reachedLastPage;

@end

@implementation PullRequestsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _repository.name;
    
    self.openedCount = self.closedCount = 0;
    
    self.page = 1;
    self.reachedLastPage = NO;
    
    [self.tableView registerNibForCellWithClass:[PullRequestCell class]];
    self.tableView.estimatedRowHeight = 125.f;
    
    self.state = PullRequestsControllerStateLoading;
    [self loadPullRequests];
}

- (void)setPullRequests:(NSArray *)pullRequests {
    _pullRequests = pullRequests;
    self.state = _pullRequests.count > 0 ? PullRequestsControllerStateContent : PullRequestsControllerStateError;
}

- (void)setOpenedCount:(NSUInteger)openedCount {
    _openedCount = openedCount;
    _openedCountLabel.text = [NSString stringWithFormat:@"%ld opened", _openedCount];
}

- (void)setClosedCount:(NSUInteger)closedCount {
    _closedCount = closedCount;
    _closedCountLabel.text = [NSString stringWithFormat:@"%ld closed", _closedCount];
}

- (void)setState:(PullRequestsControllerState)state {
    _state = state;
    [self.tableView reloadData];
}

- (void)loadPullRequests {
    [RepositoryNetworking getPullRequestsFromRepository:_repository successBlock:^(NSArray *pullRequests) {
        NSUInteger openedPartialCount = 0, closedPartialCount = 0;
        for (PullRequest *pullRequest in pullRequests) {
            pullRequest.state == PullRequestStateOpen ? openedPartialCount++ : closedPartialCount++;
        }
        self.openedCount += openedPartialCount;
        self.closedCount += closedPartialCount;
        
        self.reachedLastPage = pullRequests.count == 0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.pullRequests = _page == 1 ? pullRequests : [_pullRequests arrayByAddingObjectsFromArray:pullRequests];
            self.page++;
        });
    } failureBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.state = PullRequestsControllerStateError;
            // TODO: show retry view
        });
    }];
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _state == PullRequestsControllerStateContent ? _pullRequests.count : 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestCell *cell = [tableView dequeueReusableCellWithClass:[PullRequestCell class] forIndexPath:indexPath];
    if (_state == PullRequestsControllerStateContent) {
        PullRequest *pullRequest = _pullRequests[indexPath.row];
        
        cell.style = PullRequestCellStyleContent;
        cell.titleLabel.text = pullRequest.title;
        cell.bodyLabel.text = pullRequest.body;
        cell.userNameLabel.text = pullRequest.user.login;
        
        [cell.userImageView sd_setImageWithURL:pullRequest.user.avatarURL placeholderImage:[UIImage imageNamed:@"avatar-icon"]];
    }
    else {
        cell.style = PullRequestCellStylePlaceholder;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_state != PullRequestsControllerStateContent || indexPath.row != _pullRequests.count - 1 || _reachedLastPage) return;
    
    [self loadPullRequests];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_state != PullRequestsControllerStateContent) return;
    
    PullRequest *pullRequest = _pullRequests[indexPath.row];
    if ([[UIApplication sharedApplication] canOpenURL:pullRequest.url]) {
        [[UIApplication sharedApplication] openURL:pullRequest.url options:@{} completionHandler:nil];
    }
}

@end
