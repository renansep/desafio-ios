//
//  DICNetworking.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <AFNetworking.h>

@interface DICNetworking : AFHTTPSessionManager

+ (DICNetworking *)sharedInstance;

@end
