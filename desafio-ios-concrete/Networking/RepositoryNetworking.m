//
//  RepositoryNetworking.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "RepositoryNetworking.h"

#import "DICNetworking.h"
#import "Repository.h"
#import "PullRequest.h"

@implementation RepositoryNetworking

+ (void)getRepositoriesWithPage:(NSUInteger)page successBlock:(void (^)(NSArray *))successBlock failureBlock:(void (^)(NSError *))failureBlock {
    NSDictionary *parameters = @{@"q": @"language:Java",
                                 @"sort": @"stars",
                                 @"page": @(page)};
    [[DICNetworking sharedInstance] GET:@"search/repositories" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *parserError;
        NSArray *repositories = [Repository arrayOfModelsFromDictionaries:responseObject[@"items"] error:&parserError];
        if (repositories) {
            if (successBlock) successBlock(repositories);
        }
        else {
            if (failureBlock) failureBlock(parserError);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) failureBlock(error);
    }];
}

+ (void)getPullRequestsFromRepository:(Repository *)repository successBlock:(void (^)(NSArray *))successBlock failureBlock:(void (^)(NSError *))failureBlock {
    NSString *path = [NSString stringWithFormat:@"repos/%@/%@/pulls", repository.owner.login, repository.name];
    [[DICNetworking sharedInstance] GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *parserError;
        NSArray *pullRequests = [PullRequest arrayOfModelsFromDictionaries:responseObject error:&parserError];
        if (pullRequests) {
            if (successBlock) successBlock(pullRequests);
        }
        else {
            if (failureBlock) failureBlock(parserError);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) failureBlock(error);
    }];
}

@end
