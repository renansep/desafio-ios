//
//  RepositoryNetworking.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Repository;

@interface RepositoryNetworking : NSObject

+ (void)getRepositoriesWithPage:(NSUInteger)page successBlock:(void (^)(NSArray *repositories))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
+ (void)getPullRequestsFromRepository:(Repository *)repository successBlock:(void (^)(NSArray *pullRequests))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end
