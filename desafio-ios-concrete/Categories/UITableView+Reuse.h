//
//  UITableView+Reuse.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Reuse)

- (void)registerClassForCellWithClass:(nullable Class)cellClass;
- (void)registerNibForCellWithClass:(nullable Class)cellClass;

- (__kindof UITableViewCell * _Nonnull)dequeueReusableCellWithClass:(nullable Class)cellClass forIndexPath:(nonnull NSIndexPath *)indexPath;
- (__kindof  UITableViewCell * _Nonnull)dequeueReusableCellWithClass:(nullable Class)cellClass;

@end
