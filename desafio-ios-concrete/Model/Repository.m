//
//  Repository.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "Repository.h"

@implementation Repository

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"summary": @"description",
                                                                  @"forksCount": @"forks_count",
                                                                  @"stargazersCount": @"stargazers_count"}];
}

@end
