//
//  PullRequest.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "PullRequest.h"

@implementation PullRequest

- (void)setStateWithNSString:(NSString *)string {
    _state = [string isEqualToString:@"open"] ? PullRequestStateOpen : PullRequestStateClosed;
}

@end
