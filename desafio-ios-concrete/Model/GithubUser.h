//
//  RepositoryOwner.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <JSONModel/JSONModel.h>

typedef enum : NSUInteger {
    GihubUserTypeUser = 0,
    GihubUserTypeOrganization = 1
} GihubUserType;

@interface GithubUser : JSONModel

@property (strong, nonatomic) NSURL *avatarURL;
@property (strong, nonatomic) NSString *login;
@property (assign, nonatomic) GihubUserType type;

@end
