//
//  PullRequest.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <JSONModel/JSONModel.h>

#import "GithubUser.h"

typedef enum : NSUInteger {
    PullRequestStateOpen = 0,
    PullRequestStateClosed = 1,
} PullRequestState;

@interface PullRequest : JSONModel

@property (assign, nonatomic) PullRequestState state;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) GithubUser *user;
@property (strong, nonatomic) NSURL *url;

@end
