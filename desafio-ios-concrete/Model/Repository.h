//
//  Repository.h
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import <JSONModel/JSONModel.h>

#import "GithubUser.h"

@interface Repository : JSONModel

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *summary;
@property (strong, nonatomic) NSNumber *forksCount;
@property (strong, nonatomic) NSNumber *stargazersCount;
@property (strong, nonatomic) GithubUser *owner;

@end
