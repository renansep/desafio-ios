//
//  RepositoryOwner.m
//  desafio-ios-concrete
//
//  Created by Renan Cargnin on 3/26/17.
//  Copyright © 2017 Renan SEP. All rights reserved.
//

#import "GithubUser.h"

@implementation GithubUser

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"avatarURL": @"avatar_url"}];
}

- (void)setTypeWithNSString:(NSString *)string {
    _type = [string isEqualToString:@"User"] ? GihubUserTypeUser : GihubUserTypeOrganization;
}

@end
